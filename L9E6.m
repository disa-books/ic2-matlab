clear;close all;

% parameters
p1=0.6;
p2=0.5;
p3=-0.8;
G=[1 0 0; 0 -1 0; 0 0.5 0.5];
H=[2;1;1];
C=[1 0 1];
D=0;
dts=1;
% end parameters


%code
Mc=[H G*H G^2*H];
rank(Mc)

syms k1; syms k2; syms k3;
syms z;

solve(det(z*eye(3)-G)==0)
sys=ss(G,H,C,D,dts);
step(sys);

K=[k1 k2 k3];
zighk=z.*eye(3)-G+H*K;
lhe=det(zighk);
rhe=(z-p1)*(z-p2)*(z-p3);

eq=coeffs(lhe,z)==coeffs(rhe,z);
sol=solve(eq,[k1 k2 k3]);

solK=double([sol.k1 sol.k2 sol.k3]);
disp(solK)

altsolk=place(G,H,[p1 p2 p3]);
K=solK;

%Plot results
%normal vs controlled

figure;
sys=ss(G-H*K,H,C,D,dts);
step(sys);


Cs=eye(3); %Using identity output to check state variable time response
sys=ss(G-H*K,H,Cs,D,dts);

figure;
cl=feedback(sys,solK);
states=step(sys);

% Now, knowing state variable time response, find output using C matrix
out=C*states';
plot([states out']);
legend('x1', 'x2', 'x3' , 'y');

