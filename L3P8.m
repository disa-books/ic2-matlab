clear;
clf;
% Dibujar el lugar de las raíces para el sistema digital de la figura.
% Estudiar la estabilidad en función de K para periodos de 1, 2 y 4 segundos.

%parámetro
T=2;

%función del sistema (sin k)
num=[ T+exp(-T)-1 , 1-exp(-T)*(T+1)];
den=[conv( [1,-exp(-T)] , [1 -1] )];
G=tf( num , den , T);


rlocus(G);
%grid on;
