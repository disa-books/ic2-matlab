clear; close all;%#ok<*NOPTS>


% parameters
G=[0.5 1; -0.1 1.2];
H=[0;1];
C=[1 2];
dts=1;
p1=0;
p2=0;
% end parameters

Mo=[C;C*G]
rank(Mo)

P=poly(G); %Characteristic polynomial = |zI-G|

W=[P(2) 1 ; 1 0];
N=[C'  G'*C'];

iT=W*N';
T=inv(iT);

Go=iT*G/iT 
Ho=iT*H
Co=C/iT
%As an alternative use canon matlab function (not done here)

Moo=[Co;Co*Go];

syms k1; syms k2;
syms z;

K=[k1 ; k2];
zigkc=z.*eye(2)-Go+K*Co;
lhe=det(zigkc);
rhe=(z-p1)*(z-p2);

eq=coeffs(lhe,z, 'All')==coeffs(rhe,z, 'All');
sol=solve(eq,[k1 k2]);

solKo=double([sol.k1 ; sol.k2]);

solK=iT\solKo
