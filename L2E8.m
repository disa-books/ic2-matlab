
te=10;

w1=2*pi/8;
w2=7*2*pi/8;

dts=1%/(2*w2);

t=0:dts:te;

u1=cos(w1*t);
u2=cos(w2*t);

clf;
hold on;
plot(0:0.01:te,cos(w1*(0:0.01:te)));%real
scatter(t,u1);
plot(t,u1);
legend('real', 'scatter', 'plot');

figure;
clf;
hold on;
plot(0:0.01:te,cos(w2*(0:0.01:te)));%real
scatter(t,u2);
plot(t,u2);
legend('real', 'scatter', 'plot');



