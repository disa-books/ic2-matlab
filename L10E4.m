clear; close all;%#ok<*NOPTS>


% parameters
G=[0 1; -0.4 1.1];
H=[0;1];
C=[1.3 0.2];
D=0;
dts=1;
p1=0;
p2=0;
% end parameters

Mo=[C;C*G];
rank(Mo)

P=poly(G); %Characteristic polynomial = |zI-G|

W=[P(2) 1 ; 1 0];
N=[C'  G'*C'];

iT=W*N';
T=inv(iT);

Go=iT*G/iT
Ho=iT*H
Co=C/iT

Moo=[Co;Co*Go]

syms k1; syms k2;
syms z;

K=[k1 ; k2];
zigkc=z.*eye(2)-Go+K*Co;
lhe=det(zigkc);
rhe=(z-p1)*(z-p2);

eq=coeffs(lhe,z,'All')==coeffs(rhe,z,'All');
sol=solve(eq,[k1 k2]);

solKo=double([sol.k1 ; sol.k2]);

solK=iT\solKo

K=solK;
%Plot results
sys=ss(G,H,C,D,dts);
out=step(sys);
stairs(out)

y=0;
e=0;
u=1;
x=[0 ; 0];

hold on;
for k=1:size(out,1)
    y=C*x;
    e=out(k)-y;
    plot(k,x(1),'rs');
    plot(k,x(2),'gd');
    plot(k,y,'b*');
    
    x=G*x + H*u +  K*e;
end

hold on;


legend('y','estx1', 'estx2');
