clear; close all; %#ok<*NOPTS>

dts=1;
G=zpk([0.5 -0.3 -0.8],[0.5 1 1 -0.2],1,dts)
% G=zpk([0.5 -0.3 -0.8],[0.5 0.9 0.8 -0.2],1,dts)

% G=minreal(G)


Gtf=tf(G);
b=Gtf.Numerator{1};
a=Gtf.Denominator{1};

n=size(a,2)-1;
m=size(b,2)-1;
% an=a(1);
% bn=b(1);

S1=ss(Gtf);
[A,B,C,D]=ssdata(S1);

[T, A1]=jordan(A)
B1=inv(T)*B
C1=C*T

%controllability
Mcpart=B;
Mc=[B];
for i=2:n
    Mcpart=A*Mcpart;
    Mc=[Mc Mcpart];
end

%observabability
Mopart=C;
Mo=[C];
for i=2:n
    Mopart=Mopart*A;
    Mo=[Mo; Mopart];
end

cont_obs=[rank(Mc) rank(Mo) m n]


%plots (Be patient!!)

%uncommnet to plot jordan instead
% A=A1;B=B1;C=C1;

syms z;
x0=-300*ones(n,1);
R=inv(z*eye(n)-A);
U=dts*z/(z-1);
X=R*x0+R*B*U;
x=iztrans(X);

%If the response in time is desired
ti=0:dts:200*dts;


x1=double(subs(x,ti));

y= double( subs(C*x1,ti) );

plot (y,'b');
% plot (ti,x1,'g');
legend ('output ')

figure;
plot (ti,x1,'g');
legend ('state variables')

