clear; close all; %#ok<*NOPTS>


% parameters
p1=0.6+1i*0.6;
p2=conj(p1);
G=[-0.3 0; 1.1 1];
H=[1;0];
C=[2 1];
D=0;
dts=1;
% end parameters



%feedback calculation
Mc=[H G*H];
rank(Mc)

syms k1; syms k2;
syms z;

K=[k1 k2];
zighk=z.*eye(2)-G+H*K;
lhe=det(zighk);
rhe=(z-p1)*(z-p2);

eq=coeffs(lhe,z,'All')==coeffs(rhe,z,'All');
sol=solve(eq,[k1 k2]);

solK=double([sol.k1 sol.k2])


%altsolk=place(G,H,[p1 p2]);

%dcgain calculation
Fc=(C/subs(zighk,K,solK))*H;
sysgain=double(subs(Fc,z,1));


[num, den]=numden(Fc);
num=flip(double(coeffs(num)));
den=flip(double(coeffs(den)));
altsysgain=dcgain(tf(num,den,dts));

%Plot results
K=solK;

%Plot results
%normal vs controlled

figure;
sys=ss(G-H*K,H,C,D,dts);
step(sys);
hold on;
step(sys*(1/sysgain));

Cs=eye(2); %Using identity output to check state variable time response
sys=ss(G-H*K,H,Cs,D,dts);

figure;
cl=feedback(sys,solK);
states=step(sys);

% Now, knowing state variable time response, find output using C matrix
out=C*states';
plot([states out']);
legend('x1', 'x2', 'y');

