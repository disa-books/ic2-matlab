clear;close all;
z=tf('z');

G=((z+0.5)*(z-1.1))/((z+1.2)*(z-0.2)*(z+0.4));
B=G.Numerator{1};
A=G.Denominator{1};
Gz=roots(B)'; %openLoop Zeros
Gp=roots(A)'; %openLoop Poles


Mp=0.2;
ns=15;
T=1;

ts=ns*T;
sigma=pi/ts;
wd=-sigma*pi/log(Mp);


% Closed loop poles
Pc=exp(T*(-sigma+1i*wd));
u=[real(Pc) imag(Pc)]

% Check if gain can do
figure;pzmap(G)

Cz=[0.2 -0.4 -0.3];
Cp=[-0.5 1 -0.568];

R=zpk(Cz,Cp,-2.03,T);


% K = 1/dcgain(R*G);
% R=K*R;

RG=minreal(R*G);
F=minreal(RG/(RG+1))

figure;rlocus(RG);
figure;step(F);
grid on;
% sisotool(GlobalFunction);


