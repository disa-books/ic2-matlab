clear;close all;%#ok<*NOPTS>

% parameters
G=[0  0 1.2 ; 1 0 -0.8 ; 0 1 0.3];
H=[2.1 -0.2 1.1]';
C=[0 0 1];
D=0;
dts=1;
p1=0;
p2=0;
p3=-0.1;
% end parameters

Mo=[C;C*G;C*G^2]
N=rank(Mo)


syms k1; syms k2; syms k3;
syms z;


K=[k1; k2 ; k3];
zigkc=z.*eye(N)-G+(K*C);
lhe=det(zigkc);
rhe=(z-p1)*(z-p2)*(z-p3);

eq=coeffs(lhe,z,'All')==coeffs(rhe,z,'All');
sol=solve(eq,[k1 k2 k3]);

solK=double([sol.k1 sol.k2 sol.k3])

K=solK';
%Plot results
sys=ss(G,H,C,D,dts);
out=step(sys,50);
stairs(out)

y=0;
e=0;
u=1;
x=[0 ; 0; 0];

hold on;
for k=1:size(out,1)
    y=C*x;
    e=out(k)-y;
    plot(k,x(1),'rs');
    plot(k,x(2),'gd');
    plot(k,x(3),'mo');
    plot(k,y,'b*');
    
    x=G*x + H*u +  K*e;
end

hold on;


legend('y','estx1', 'estx2', 'estx3');
