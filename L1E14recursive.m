clear;
clf;
%variable k
hold on;
dts=1
t=0:dts:50;
i=1:size(t,2);


%Primero hay que calcular la transformada z de la expresión teniendo en cuenta la
% propiedad de translación real o temporal y la transformada de u ( k ) ya que es
% conocida.

%2x(k)-2x(k-1)+x(k-2)=u(k)

u=ones(size(t));
x=zeros(size(t));

x(1)=(1+0+0)/2;
x(2)=(1+2*x(1)-0)/2;

for k=3:size(t,2)

x(k)=(u(k)+2*x(k-1)-x(k-2))/2;

end

plot(t,x,'-');

%Solución obtenida en papel








