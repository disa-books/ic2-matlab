clear;%#ok<*NOPTS>
syms z;

G=[ -1 0 0;
    1 2 1;
    0 0 3];

%H=[ 0 2 1 ]';
H=[ 0 2 1 ]';

%C=[3 0 1];
C=[3 0 1];
dts=1;

P=ss(G,H,C,0,dts)
[b,a]=ss2tf(G,H,C,0)
Ptf=tf(b,a,dts)
zpk(Ptf)
minreal(Ptf/tf(P));



%% diagonal from exercise

%undetermined:
%undefined variables
v11=-3;v22=1;v33=1;
%dependant variables (see exercise)
v12=-v11/3;v32=v33;

T=[ v11  0   0;
    v12 v22 v32;
     0   0  v33];

Ge=inv(T)*G*T
He=inv(T)*H
Ce=C*T;
Pe=ss(Ge,He,Ce,0,dts);
minreal(Ptf/tf(Pe));

%% using: T*H' = H when H'=[1 1 1]'
%v11+v21+v31=0 -> v11=0, but T has no inverse.
% We must discard to force V11=0.
% There is no way to get inv(T)*H=[1 1 1]', but:
%v12+v22+v32=2 -> v22=2-v32-v12
%v13+v23+v33=1 -> v33=1

%Still undefined: v22
v22=2;
%Defined using T*H' = H and eigenvalues
v33=1;
v32=v33;
v12=2-v32-v22;
v11=-3*v12;
T=[ v11  0   0;
    v12 v22 v32;
     0   0  v33];

Ge=inv(T)*G*T
He=inv(T)*H
Ce=C*T


%% diagonal using transfer function
[r,p,k] = residue(b,a)
Gd=diag(p)
Hd=ones(3,1)
Cd=r'

Pdef=ss(Gd,Hd,Cd,0,1);
%zI_Gd=z*eye(3)-Gd;
%Pdtf=Cd*inv(zI_Gd)*Hd
%minreal(Ptf/tf(Pd))



%% diagonal using T form matlab eig() function

[Tm,Gm] = eig(G);

inv(Tm)*G*Tm
inv(Tm)*H
