clear; %#ok<*NOPTS>
G=[ -1   0  0  ; 0  2   0 ; 0  0 -2];
H=[2;1;0];

Mc=[H G*H G^2*H]
rank(Mc)

x0=[0 0 0]';
x3=[3 2 2]';

u=[-1/3;-5/6;1]

x=G^3*x0+Mc*u

