
n=[1 1 2];%necesario z^-1 (z^2+z+2)
d1=[1 -1 1];
d2=[1 -1];
d=conv(d1,d2);

p=roots(d1);
p1=p(1);
p2=p(2);

%1-fracciones simples para el polo z=1
%replace z=1 at num and den
nA=polyval(n,1);
dA=polyval(d1,1);
%result
A=nA/dA;

%1-fracciones simples para el polo z=p1
nB=(p1^2+p1+2)
dB=conv((p1-1),(p1-p2))
B=nB/dB

angle(B)
abs(B)

k=0:0.01:10;
%la solución tiene z^-1, así que f se evalúa en k-1
f=A+B*p1.^(k-1)+conj(B)*p2.^(k-1);
%f=4+B*p1.^(k)+conj(B)*p2.^(k);
plot(k,f);
hold on;
%Solución obtenida en papel
g=4 - 3.06*cos( (k-1)*pi/3+0.19 );
h=4 - 3.06*cos( (k)*pi/3-0.765 );
plot(k,g,'.');
plot(k,h,'-');
legend('f direct solution','g paper solution 1 (k-1)','h paper solution 2 (k)')