clear;close all;
s=tf('s');

G=1/((s+1)*(s+2)*(s+4)*(s+5));
B=G.Numerator{1};
A=G.Denominator{1};
Gz=roots(B)'; %openLoop Zeros
Gp=roots(A)'; %openLoop Poles


Mp=0.2;
ts=2.1;

sigma=pi/ts;
wd=-sigma*pi/log(Mp);


% Closed loop poles
Pc=-sigma+1i*wd;
u=[real(Pc) imag(Pc)]

% Check if gain can do
figure;hold on;
rlocus(G);plot(Pc,'*');

%%
% PD s+c1
% control definition
%PD pole at zero
Cp=[]; % No poles

% Angle criterion
poles=Pc-[Gp Cp];
Sp=sum(angle(poles));
Sz = Sp - pi;
b=imag(Pc)/tan(Sz);
c1=real(Pc)-b;
Cz=c1;
zeros=Pc-[Gz Cz];

% Module criterion
moduleK = prod(abs(poles))/prod(abs(zeros));

C=zpk(Cz,Cp,moduleK);


figure;hold on;
rlocus(C*G);plot(Pc,'*');

%%
figure;hold on;
step(feedback(C*G,1));
grid on;


%% PID for stationary error
c2= -sigma/6;
Cz=[Cz c2];

% Sz = Sp+atan2(imag(Pc),real(Pc)) - pi;
% b=imag(Pc)/tan(Sz/2);
% c2=real(Pc)-b;
% Cz=[c2 c2];


Cp=[Cp 0];

moduleK=moduleK*abs(Pc-0)/abs(Pc-c2);

C=zpk(Cz,Cp,moduleK);


figure;hold on;
rlocus(C*G);plot(Pc,'*');

%%

figure;hold on;
step(feedback(C*G,1));
grid on;

%%
CG=Pc*conj(Pc)/( (s-Pc)*(s-conj(Pc)) )
step(CG);
grid on;

% sisotool(G,C);





