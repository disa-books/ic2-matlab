clear;close all;%#ok<*NOPTS>

% parameters
G=[-0.8 1; 0 0.5];
H=[0 1]';
C=[1 2];
D=0;
dts=1;
p1=0;
p2=0;
% end parameters

Mo=[C;C*G] 
N=rank(Mo)


syms k1; syms k2;
syms z;


K=[k1; k2];
zigkc=z.*eye(N)-G+(K*C);
lhe=det(zigkc);
rhe=(z-p1)*(z-p2);

eq=coeffs(lhe,z,'All')==coeffs(rhe,z,'All');
sol=solve(eq,[k1 k2]);

solK=double([sol.k1 sol.k2])

K=solK';
%Plot results
sys=ss(G,H,C,D,dts);
out=step(sys);
stairs(out)

y=0;
e=0;
u=1;
x=[0 ; 0];

hold on;
for k=1:size(out,1)
    y=C*x;
    e=out(k)-y;
    plot(k,x(1),'rs');
    plot(k,x(2),'gd');
    plot(k,y,'b*');
    
    x=G*x + H*u +  K*e;
end

hold on;


legend('y','estx1', 'estx2');
