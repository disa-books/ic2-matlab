clear;close all;
z=tf('z');

G=1/((z-0.7)*(z-0.9));
B=G.Numerator{1};
A=G.Denominator{1};
Gz=roots(B)'; %openLoop Zeros
Gp=roots(A)'; %openLoop Poles


Mp=0.2;
n=15;
T=1;

ts=n*T;
sigma=pi/ts;
wd=-sigma*pi/log(Mp);


% Closed loop poles
Pc=exp(T*(-sigma+1i*wd));
u=[real(Pc) imag(Pc)]

% Check if gain can do
figure;rlocus(tf(B,A,T))

%%
% PD (z-c)/z
% control definition
%PD pole at zero
Cp=[]; %1/z

%angle criterion
poles=Pc-[Gp Cp];
Sp=sum(angle(poles));
Sz = Sp - pi;
b=atan(Sz);
c1=real(Pc)-b;
Cz=c1;

C=zpk(Cz,Cp,1,T);


zeros=Pc-[Gz Cz];

moduleK = prod(abs(poles))/prod(abs(zeros));

C=moduleK*C;

figure;rlocus(C*G);
figure;step(feedback(C*G,1));
grid on;
% sisotool(GlobalFunction);


