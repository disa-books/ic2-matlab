clear;
clf;

% parameters
p1=0.6+1i*0.6;
p2=conj(p1);
G=[-1 0.5; 0.2 1];
H=[1 0.5; 0 0];
C=[0 2];
D=[0 0 ; 0 0];
dts=0.1;
% end parameters


%code
Mc=[H G*H];
rank(Mc)

syms k1; syms k2; syms k3; syms k4;
syms z;

K=[k1 k2 ; k3 k4];
zighk=z.*eye(2)-G+H*K;
lhe=det(zighk);
rhe=(z-p1)*(z-p2);

eq=coeffs(lhe,z)==coeffs(rhe,z);
sol=solve(eq,[k1 k2]);


forcek3k4=[1 1];
solK=double(subs([sol.k1 sol.k2],[k3 k4],forcek3k4 ));
solK=[solK; forcek3k4];
disp(solK)

altsolk=place(G,H,[p1 p2]);



%Plot results using C=eye(2) to get states as sytem output.
sys=ss(G,H,eye(2),D,dts);
dcgain=1;
cl=(1/dcgain)*feedback(sys,solK);
states=step(cl);
states1=states(:,:,1);
states2=states(:,:,2);

outu1=C*states1';
outu2=C*states2';

subplot(3,1,1);
plot([states1 outu1']);
legend('x1', 'x2', 'y(u1)');

subplot(3,1,2);
plot([states2  outu2']);
legend('x1', 'x2', 'y(u2)');

subplot(3,1,3);
plot([states1+states2  outu1'+outu2']);
legend('totalx1', 'totalx2', 'y(u1+u2)');

