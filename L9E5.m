clear;

% parameters
p1=0.2;
p2=0.5;
G=[1 0.5; -0.5 2];
H=[1;0];
C=[2 1];
D=0;
dts=1;
% end parameters



%code
Mc=[H G*H];
rank(Mc)

syms k1; syms k2;
syms z;

solve(det(z*eye(2)-G)==0)
sys=ss(G,H,C,D,dts);
step(sys);

K=[k1 k2];
zighk=z*eye(2)-G+H*K;
lhe=det(zighk);
rhe=(z-p1)*(z-p2);

eq=coeffs(lhe,z)==coeffs(rhe,z);
sol=solve(eq,[k1 k2]);

solK=double([sol.k1 sol.k2]);
disp(solK)

altsolk=place(G,H,[p1 p2]);
K=solK;

%Plot results
%normal vs controlled

figure;
sys=ss(G-H*K,H,C,D,dts);
step(sys);


Cs=eye(2); %Using identity output to check state variable time response
sys=ss(G-H*K,H,Cs,D,dts);

figure;
cl=feedback(sys,solK);
states=step(sys);

% Now, knowing state variable time response, find output using C matrix
out=C*states';
plot([states out']);
legend('x1', 'x2', 'y');





