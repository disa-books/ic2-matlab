clear; %#ok<*NOPTS>
G=[ 1   0  0  ; 0 -1   0 ; 0  0 2];
H=[1;2;1];

Mc=[H G*H G^2*H]
rank(Mc)

x0=[0 1 0]';
x3=[1 1 2]';
u=  inv(Mc) * (x3 - G^3*x0);

u'