clear;
clf;
% parameters
G=[0.8 -0.5; 1 0.5];
H=[0.6;0.1];
C=[1 0];
D=0;
dts=1;
p1=0;
p2=0;
% end parameters

Mo=[C;C*G];
rank(Mo)


syms k1;
syms z;

N=2;M=1;
Gmm=G(1:M,1:M);
Gnn=G(M+1:N,M+1:N);
Gnm=G(M+1:N,1:M);
Gmn=G(1:M,M+1:N);
Hm=H(1);
Hn=H(M+1:N);
K=k1;
zigkc=z.*eye(N-M)-(Gnn-K*Gmn);
lhe=det(zigkc);
rhe=(z-p1);

eq=coeffs(lhe,z,'All')==coeffs(rhe,z,'All');
sol=solve(eq,k1);

solK=double(sol);
%solK=double([sol.k1]);

K=solK;
%Plot results
sys=ss(G,H,C,D,dts);
out=step(sys);
stairs(out)

y=0;
e=0;
u=1;
x=[0;0];
hold on;
for k=1:size(out,1)-1
    %observable part
    x(1)=out(k);
    xx=out(k+1);
    
    y=C*x;
    
    e=out(k)-y;
    plot(k,x(1),'rs');
    plot(k,x(2),'gd');
    plot(k,y,'b*');
    
    %non-observable part
    Key=K*(xx-Gmm*x(1)-Hm*u);
    x(2)=(Gnn-K*Gmn)*x(2) +Gnm*x(1) + Hn*u +  Key;
end

hold on;


legend('y','measuredx1', 'estimatedx2');

showC=[1 0;0 1];

figure;
%Plot results
sys=ss(G,H,showC,D,dts);
states=step(sys);
out=C*states';
plot([states out']);
legend('x1', 'x2', 'y');
