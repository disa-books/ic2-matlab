clear;
clf;
%variable k
hold on;
dts=1
k=0:dts:50;


%Primero hay que calcular la transformada z de la expresión teniendo en cuenta la
% propiedad de translación real o temporal y la transformada de u ( k ) ya que es
% conocida.

%(G/z)=(z^2)/( (z-1) * (2z^2-2z+1) )
num=[ 1 0 0 ]; %(z^2)
den1=[1 -1]; %(z-1)
den2=2*[1 -1 0.5]; %(2z^2-2z+1)
%den = (z-1)*(2z^2-2z+1)
den=conv(den1,den2)

% Esta expresión se puede expandir en fracciones parciales. Los coeficientes se
% obtendrán por igualación:
%get poles of (2z^2-2z+1)
poles = roots(den2)
p2 = poles(2)
p1 = poles(1)

%1-fracciones simples para el polo z=1
%pole z-1 ---> (z-1) / ( 2*(z-1)*(z-p1)*(z-p2) )
%pole z-1 ---> 1 / ( 2*(z-p1)*(z-p2) )
denA=2*conv([1 -p2],[1 -p1]);
%replace z=1 at num and den
nA=polyval(num,1);
dA=polyval(denA,1);
%result
A=nA/dA;

%2-fracciones simples para el polo z=p1
%pole z-p1 ---> (z-p1) / ( 2*(z-1)*(z-p1)*(z-p2) )
%pole z-p1 ---> 1 / ( 2*(z-1)*(z-p2) )
denB=2*conv([1 -1],[1 -p2]);
%replace z=-p1 at num and den
nB=polyval(num,p1);
dB=polyval(denB,p1);
%esult
B=nB/dB;

%3-fracciones simples para el polo z=p2
%conjugado de B
C=conj(B);


%Solución por tablas para ( A*z/(z-1) + B*z/(z-p1) + C*z/(z-p2) )
f= ( A + B*p1.^(k) + C*p2.^(k) );
%f= ( A + B*p1.^(k*dts) + C*p2.^(k*dts) );
plot(k,f,'-');

%Solución obtenida en papel
g=1 - (sqrt(2)/2).^(k+1) .* cos( (k+1)*pi/4 );
%g=1 - (sqrt(2)/2)*(sqrt(2)/2).^k .* cos( k*pi/4+pi/4 );
plot(k,g,'.');








