clear; %#ok<*NOPTS>
G=[ -1   0  0  ; 0 -2   0 ; 1  0 -1];
H=[1;1;1];
C=[1   1   2 ; 0   1   -2];

Mco=[C*1*H C*G*H C*G^2*H]
rank(Mco)

x0=[0 0 0]';
x3=[1 1 2]';

y3=[2 2]';

M=Mco(1:2,1:2);N=Mco(1:2,3);


%% result
u0=1;
u=  inv(M) * (y3 - C*G^3*x0 -N*u0) ;

u=[u ;u0]

y=C*G^3*x0+Mco*u