clear;close all;
clf;
%'pkg load control'

% 
% Dibujar el lugar de las raíces para el sistema con controlador digital de la figura
% teniendo en cuenta que la función de transferencia del controlador viene dada por
% la siguiente expresión:

% C ( z ) = K*z / (z − 1)



%parámetro de entrada: Sampling time y k
T=1;
%T=1;
k=1;


%solución papel
%G = (1-e^-T) / (z-p1)
numG=[1-exp(-T)];    %  (1-e^-T)
                     % ---------
denG=[1 , -exp(-T)]; %  (z-p1)
G=tf( numG , denG , T);

rlocus(G);

%C = k*z / (z-1)
numC= [ 1 , 0]; %   z  (sin tener en cuenta k)
                % -----
denC= [1 , -1]; % (z-1)
C=tf( numC , denC , T);

CG = C*G
%CG = tf([1 0.4],[1 -1.3 0.3 0],T);

fig= figure; rlocus(CG);
grid on;

%manually print root locus for k actual value
N=conv( numC , numG );
D=conv( denC , denG );


hold on;
for k=0:0.1:10
w = waitforbuttonpress;

pause(0.1);
%Teniendo D y N para el lazo abierto, la función de transferencia
%será CL = (k*N/D)/(1+k*N/D) = k*N/(D+k*N)
N=[zeros(size(D,2)-size(N,2)), N]; %zero pad array N para D+k*N
P=D+k*N;

%poles= roots(P);p1=poles(1);p2=poles(2);
%Se puede hacer con roots, pero vamos a ver el cómo afecta el signo a RL
%usando la solución de segundo grado z=(-b+sqrt(b^2-4ac))/2a.
p1=(-P(2)+sqrt(P(2)^2-4*P(1)*P(3))) / (2*P(1));
p2=(-P(2)-sqrt(P(2)^2-4*P(1)*P(3))) / (2*P(1));

%plot pole
p=p1;
p=p+1i*10^-10; %add tiny complex i for correct plot of real only values.
plot(p,'p');
legend(num2str(p1));

end
% 
% 


