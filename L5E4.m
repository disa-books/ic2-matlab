clear;close all;
z=tf('z');

G=z/((z-1)*(z-2));
B=G.Numerator{1};
A=G.Denominator{1};
Gz=roots(B)'; %openLoop Zeros
Gp=roots(A)'; %openLoop Poles

K=1.98;
c1=0.75;
C=K*(z-c1)/z;

figure;rlocus(C*G);
figure;step(feedback(C*G,1));
grid on;
% sisotool(GlobalFunction);


